#!/bin/bash

curses_ipk=libncursesw5_5.9-r15.1_aarch64.ipk
smartcols_ipk=libsmartcols1_2.26.2-r0_aarch64.ipk
systemd_ipk=libsystemd0_225+git0+e1439a1472-r0_aarch64.ipk
losetup_ipk=util-linux-losetup_2.26.2-r0_aarch64.ipk
swaponoff_ipk=util-linux-swaponoff_2.26.2-r0_aarch64.ipk
umount_ipk=util-linux-umount_2.26.2-r0_aarch64.ipk
util_ipk=util-linux_2.26.2-r0_aarch64.ipk
aufs_ipk=aufs-util_3.14+git0+b59a2167a1-r0_aarch64.ipk
cgroup_ipk=libcgroup_0.41-r0_aarch64.ipk
cgroup_lite_ipk=cgroup-lite_1.1-r0_aarch64.ipk
docker_ipk=docker_1.9.0+git76d6bc9a9f1690e16f3721ba165364688b626de2-r0_aarch64.ipk
