#!/bin/bash

source package-names.sh

# Install Docker and all dependencies in the correct order
opkg install $curses_ipk
opkg install $smartcols_ipk
opkg install $systemd_ipk
opkg install $losetup_ipk
opkg install $swaponoff_ipk
opkg install $umount_ipk
opkg install $util_ipk
opkg install $aufs_ipk
opkg install $cgroup_ipk
opkg install $cgroup_lite_ipk
opkg install $docker_ipk

cp docker-start.service /etc/systemd/system
systemctl enable docker-start.service

cp prepare-docker.service /etc/systemd/system
systemctl enable prepare-docker.service

mkdir -p /etc/modalai
cp hw.tgz /etc/modalai
cp prepare-docker.sh /etc/modalai
