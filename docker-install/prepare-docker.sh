#!/bin/bash

if [ ! -f "/var/run/resolv.conf" ]; then
	/bin/echo "nameserver 8.8.8.8" > /var/run/resolv.conf
fi

/usr/bin/docker load -i /etc/modalai/hw.tgz
/usr/bin/docker run aarch64/hello-world:latest
/bin/echo "0-3" > /sys/fs/cgroup/cpuset/system.slice/cpuset.cpus

